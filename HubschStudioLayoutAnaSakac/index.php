<?php
/**
 * Created by PhpStorm.
 * User: tomislavfabeta
 * Date: 4/2/17
 * Time: 6:15 PM
 */
?>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
        <link REL=StyleSheet HREF="static/css/menuHamburger.css" TYPE="text/css" MEDIA=screen>
        <link REL=StyleSheet HREF="static/css/style.css" TYPE="text/css" MEDIA=screen>
    </head>
    <body>

    <div class="container">
        <?php
            include("site/index.php");
        ?>
    </div>


    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <script src="static/js/jquery.hover3d.js"></script>
    <script src="static/js/smallGallery.js"></script>
    <script src="static/js/menu.js"></script>
<!--    Design by: Ana Sakač-->
<!--    Link: https://dribbble.com/shots/3260099-Hubsch-Studio-Layout-->
    </body>
</html>
