<div class="container">
    <div class="inner_container">
        <div class="grid">
            <div class="menu column">
                <div class="menu_container">
                    <div class="menu_item">
                        <div class="menu_active_line"></div>
                        Storage
                    </div>
                    <div class="menu_item">
                        <div class="menu_active_line"></div>
                        Organizers
                    </div>
                    <div class="menu_item">
                        <div class="menu_active_line"></div>
                        Glass Displays
                    </div>
                    <div class="menu_item active">
                        <div class="menu_active_line"></div>
                        Furniture
                    </div>
                    <div class="menu_item">
                        <div class="menu_active_line"></div>
                        Mirrors
                    </div>
                    <div class="menu_item">
                        <div class="menu_active_line"></div>
                        Candlelight
                    </div>
                </div>
            </div>
            <div class="main_gallery column">
                <div class="main_gallery_container">
                    <div class="main_gallery_item">
                        <img src="https://s24.postimg.org/t1sognucx/furniture1.png">
                    </div>
                </div>
            </div>
            <div class="small_gallery column">

                <div class="small_gallery_container">
                    <div class="small_gallery_relative_container">
                        <div class="small_gallery_absolute_container">
                            <div class="custom_gallery">
                                <div class="custom_gallery_pager">
                                    <div class="custom_gallery_pager_items">

                                    </div>
                                </div>
                                <div class="custom_gallery_items_container">
                                    <div class="custom_gallery_items">
                                        <div class="custom_gallery_item">
                                            <div class="custom_gallery_item_img">
                                                <img src="https://s24.postimg.org/t1sognucx/furniture1.png">
                                            </div>
                                            <div class="custom_gallery_description">
                                                Darcy Sofa Chaise
                                                <div class="button icon arrow right"></div>
                                            </div>
                                        </div>
                                        <div class="custom_gallery_item">
                                            <div class="custom_gallery_item_img">
                                                <img src="https://s24.postimg.org/bzzuekfht/darci_sofa_chaise.png">
                                            </div>
                                            <div class="custom_gallery_description">
                                                Darcy Sofa Chaise
                                                <div class="button icon arrow right"></div>
                                            </div>
                                        </div>
                                        <div class="custom_gallery_item">
                                            <div class="custom_gallery_item_img">
                                                <img src="https://s24.postimg.org/bzzuekfht/darci_sofa_chaise.png">
                                            </div>
                                            <div class="custom_gallery_description">
                                                Darcy Sofa Chaise
                                                <div class="button icon arrow right"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="hamburger">
                    <div id="nav-icon2">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script src="static/js/jquery.hover3d.js"></script>
<script src="static/js/smallGallery.js"></script>
<script src="static/js/menu.js"></script>
