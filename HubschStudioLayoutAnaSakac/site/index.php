<?php
/**
 * Created by PhpStorm.
 * User: tomislavfabeta
 * Date: 4/2/17
 * Time: 7:06 PM
 */
?>

<div class="site_container">
    <div class="inner_container">
        <div class="grid">
            <div class="menu column">
                <?php
                include("site/menu.php");
                ?>
            </div>
            <div class="main_gallery column">
                <?php
                include("site/mainGallery.php");
                ?>
            </div>
            <div class="small_gallery column">
                <?php
                include("site/smallGallery.php");
                ?>
            </div>
        </div>

    </div>
</div>