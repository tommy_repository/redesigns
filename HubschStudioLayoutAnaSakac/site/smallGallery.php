<?php
/**
 * Created by PhpStorm.
 * User: tomislavfabeta
 * Date: 4/2/17
 * Time: 7:10 PM
 */
?>

<div class="small_gallery_container">
    <div class="small_gallery_relative_container">
        <div class="small_gallery_absolute_container">
            <div class="custom_gallery">
                <div class="custom_gallery_pager">
                    <div class="custom_gallery_pager_items">

                    </div>
                </div>
                <div class="custom_gallery_items_container">
                    <div class="custom_gallery_items">
                        <div class="custom_gallery_item">
                            <div class="custom_gallery_item_img">
                                <img src="static/images/darci_sofa_chaise.png">
                            </div>
                            <div class="custom_gallery_description">
                                Darcy Sofa Chaise
                                <div class="button icon arrow right"></div>
                            </div>
                        </div>
                        <div class="custom_gallery_item">
                            <div class="custom_gallery_item_img">
                                <img src="static/images/furniture1.png">
                            </div>
                            <div class="custom_gallery_description">
                                Darcy Sofa Chaise
                                <div class="button icon arrow right"></div>
                            </div>
                        </div>
                        <div class="custom_gallery_item">
                            <div class="custom_gallery_item_img">
                                <img src="static/images/darci_sofa_chaise.png">
                            </div>
                            <div class="custom_gallery_description">
                                Darcy Sofa Chaise
                                <div class="button icon arrow right"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="hamburger">
    <div id="nav-icon2">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
