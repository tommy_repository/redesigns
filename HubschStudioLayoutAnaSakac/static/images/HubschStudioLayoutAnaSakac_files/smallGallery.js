/**
 * Created by tomislavfabeta on 4/2/17.
 */

(function($){
    function CustomGallery(){
        this.$gallery = {
            'absolute_container': '.small_gallery_absolute_container',
            'container': '.custom_gallery',
            'pager': '.custom_gallery_pagger',
            'item': '.custom_gallery_item',
        };
        this.window = {
            height: $(window).height,
            width: $(window).width
        };

        this.init();
    }

    CustomGallery.prototype.init = init;
    CustomGallery.prototype.renderImg = renderImg;
    CustomGallery.prototype.selectImage = selectImage;
    CustomGallery.prototype.positionGallery = positionGallery;

    /**
     * Public functions
     */
    function init(){
        this.positionGallery()
    }

    function renderImg(){

    }

    function selectImage(){

    }

    function positionGallery(){
        var galleryTopPosition = this.window.height - (this.window.height * 0.1);
        console.log(galleryTopPosition);
        $(this.$gallery.absolute_container).css("top", galleryTopPosition+"px");
    }


    new CustomGallery();
})($);
