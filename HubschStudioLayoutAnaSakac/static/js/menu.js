/**
 * Created by tomislavfabeta on 5/2/17.
 */
(function($){
    $(".menu_item").on('click', function () {
        $(".menu_item").removeClass('active');
        $(this).addClass("active");
    });
    $(document).ready(function(){
        $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
            $(this).toggleClass('open');
        });
    });

    $(window).resize(function(){
        $('.site_container').height($("body").height()-$("body").height()*0.05);
    });

    $('.site_container').height($("body").height()-$("body").height()*0.05);
})($);