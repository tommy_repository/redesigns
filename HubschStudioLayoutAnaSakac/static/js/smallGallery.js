/**
 * Created by tomislavfabeta on 4/2/17.
 */

(function($){
    function CustomGallery(){
        this.CLASS = {
            'active': 'active',
        };
        this.$gallery = {
            'absolute_container': '.small_gallery_absolute_container',
            'container': '.custom_gallery',
            'pager': '.custom_gallery_pager',
            'pagerItems': '.custom_gallery_pager_items',
            'pagerItem': '.custom_gallery_pager_item',
            'pagerItemLine': '.custom_gallery_pager_item_line',
            'item': '.custom_gallery_item',
            'items': '.custom_gallery_items',
            'img': '.custom_gallery_item_img',
        };
        this.window = {
            height: (function(){return $(window).height()}),
            width: (function(){return $(window).width()}),
        };

        this.index = 0;
        this.init();
    }

    CustomGallery.prototype.init = init;
    CustomGallery.prototype.events = events;
    CustomGallery.prototype.setControls = setControls;
    CustomGallery.prototype.renderImg = renderImg;
    CustomGallery.prototype.selectImage = selectImage;
    CustomGallery.prototype.customizeGallery = customizeGallery;

    /**
     * Public functions
     */
    function init(){
        this.customizeGallery();
        this.setControls();
        this.events();
        this.selectImage(0);
    }

    function events(){
        var self = this;
        $(window).resize(function(){
            self.customizeGallery();
        });
        $(this.$gallery.pagerItem).each(function () {
            $(this).on('click', function () {
                self.selectImage($(this).data('index'));
            });
        });
    }

    function renderImg(index){
        // $(this.$gallery.item).each(function(){
        //     var dataIndex = $(this).data("index");
        //
        // });
    }

    function selectImage(index){
        var self = this;
        $(this.$gallery.item).each(function(){
            var dataIndex = $(this).data("index");

            if (parseInt(index) == parseInt(dataIndex)){
                $(this).addClass(self.CLASS.active);
                self.renderImg(index);
            } else{
                $(this).removeClass(self.CLASS.active);
            }
        });

        $(this.$gallery.pagerItem).each(function(){
            var dataIndex = $(this).data("index");

            if (parseInt(index) == parseInt(dataIndex)){
                $(this).addClass(self.CLASS.active);
            }else{
                $(this).removeClass(self.CLASS.active);
            }
        });

    }

    function setControls(){
        var self = this;
        var control = '<div class="'+this.$gallery.pagerItem.replace('.',' ')+'" data-index="{{index}}">{{pagerText}} <div class="'+this.$gallery.pagerItemLine.replace('.', '')+'"></div></div>';
        var controls = [];

        var index = 0;
        $(this.$gallery.items).children().each(function(){
            var preparedControl = control;
            var params = {
                index: index,
                pagerText: '0' + (index +1)
            };

            for (var key in params){
                var regex = new RegExp("{{" + key + "}}", "g");
                preparedControl = preparedControl.replace(regex, params[key]);
            }

            controls.push(preparedControl);
            $(this).data('index', index);
            index++;
        });

        $(this.$gallery.pagerItems).html(controls.join(""));
    }

    function customizeGallery(){
        var itemSize = calculateItemsSize($(this.$gallery.items));
        var itemsCss = {
            width: "10000px",
            height: "350px",
        };
        var containerCss = {
            'width': itemSize+'px'
        };
        $(this.$gallery.items).css(itemsCss);
        $(this.$gallery.container).css(containerCss);
    }


    new CustomGallery();

    function calculateItemsSize($items){
        var size = 0;

        $items.children().each(function(){
             size = $(this).width();
        });

        return size;
    }
})($);
